<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "gameGenre".
 *
 * @property integer $id
 * @property string $name
 *
 * @property Game[] $games
 */
class GameGenre extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'gameGenre';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGames()
    {
        return $this->hasMany(Game::className(), ['genreID' => 'id']);
    }

    /**
     * @inheritdoc
     * @return GamesQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new GamesQuery(get_called_class());
    }
}
