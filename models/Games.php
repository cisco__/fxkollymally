<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "game".
 *
 * @property integer $id
 * @property string $name
 * @property string $createDate
 * @property integer $creatorId
 * @property string $gameCover
 * @property string $descr
 * @property string $developer
 * @property string $publisher
 * @property integer $genreID
 *
 * @property Users $creator
 * @property GameGenre $genre
 * @property GameComments[] $gameComments
 */
class Games extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'game';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'createDate', 'creatorId', 'gameCover', 'descr', 'developer', 'publisher', 'genreID','youtubeLink'], 'required'],
            [['createDate'], 'safe'],
            [['creatorId', 'genreID'], 'integer'],
            [['descr'], 'string'],
            [['name', 'developer', 'publisher'], 'string', 'max' => 50],
            [['gameCover'], 'string', 'max' => 100],
            [['youtubeLink'], 'string', 'max' => 400]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'createDate' => 'Create Date',
            'creatorId' => 'Creator ID',
            'gameCover' => 'Game Cover',
            'descr' => 'Descr',
            'developer' => 'Developer',
            'publisher' => 'Publisher',
            'genreID' => 'Genre ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreator()
    {
        return $this->hasOne(Users::className(), ['id' => 'creatorId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGenre()
    {
        return $this->hasOne(GameGenre::className(), ['id' => 'genreID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGameComments()
    {
        return $this->hasMany(GameComments::className(), ['gameID' => 'id']);
    }

    /**
     * @inheritdoc
     * @return GameQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new GameQuery(get_called_class());
    }
}
