<?php

$params = require(__DIR__ . '/params.php');

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'components' => [
    'authClientCollection' => [
            'class' => 'yii\authclient\Collection',
            'clients' => [
                'google' => [
                                'class' => 'yii\authclient\clients\GoogleOpenId'
                            ],
                'facebook' => [
                                'class' => 'yii\authclient\clients\Facebook',
                                'authUrl' => 'https://www.facebook.com/dialog/oauth?display=popup',
                                'clientId' => '436143989908751',
                                'clientSecret' => 'e45be2e2b49672bc449be6e8434d6408',
                            ],
                        ],
                    ],
    'urlManager' => [

            'showScriptName' => false,
            'enablePrettyUrl' => true,
            
            'rules' => array(
                  '<controller:\w+>/<id:\d+>' => '<controller>/view',
                  '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                  '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
                  '/' => 'site/index',
                  'login' => 'users/login',
                  'createUser' => 'users/create',
                  'createGames' => 'games/create',
                  'logout' => 'users/logout',
            ),
            
         ],
         'mailer' => [
                        'class' => 'yii\swiftmailer\Mailer',
                        'transport' => [
                            'class' => 'Swift_SmtpTransport',
                            'host' => 'smtp.gmail.com',
                            'username' => 'ifca.acfi.tt',
                            'password' => '@Ifc123456',
                            'port' => '587',
                            'encryption' => 'tls',
                                        ],
                    ],
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'maninblack',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'db'=>[
            'class'=>'yii\db\Connection',
            'dsn' => 'mysql:host=localhost;dbname=gamesdb',
            'username' => 'root',
            'password' => 'ciscokeren',
            'charset' => 'utf8'            
        ],
        'user' => [
            'identityClass' => 'app\models\Users',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => require(__DIR__ . '/db.php'),
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return $config;
