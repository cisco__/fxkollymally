<?php

namespace app\controllers;

use Yii;
use app\models\Games;
use app\models\GameGenre;
use app\models\GameComments;
use app\models\gamesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\UploadedFile;
use yii\helpers\ArrayHelper;
use yii\db\Query;


/**
 * GamesController implements the CRUD actions for Games model.
 */
class GamesController extends Controller
{

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Games models.
     * @return mixed
     */
    public function actionIndex()
    {
        if (Yii::$app->request->isAjax) 
        {
            $session = Yii::$app->session;
            $request = Yii::$app->request;
            $comments = $request->post('comments'); 
            $gameID = $request->post('game'); 
            $model= new GameComments;
            $model->gameID=$gameID;
            $model->comments=$comments;
            $model->userID=$session['idUser'];
            $model->createDate=date('Y-m-d:H:i:s');
            $model->save(false);
            echo json_encode("OK");
        }
        $searchModel = new gamesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'model' => $this->getAllData(),
        ]);
        


    }

    function getAllData()
    {
        return Games::find()->all();
    }   


    public function get3data()
    {
        return Games::find()->orderBy(['id'=>SORT_DESC,])->limit(3)->all();
    }
    /**
     * Displays a single Games model.
     * @param integer $id
     * @return mixed
     */


    public function actionView($id)
    {
        $data = $this->findModel($id);
        $customer = GameGenre::findOne($data->genreID);
        $genre = $customer->name;
        return $this->render('view', [
            'model' => $this->findModel($id),
            'genre' => $genre,
            'comments' => GameComments::find()->where(['gameID' => $data->id])->all(),
        ]);
    }

    /**
     * Creates a new Games model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $session = Yii::$app->session;
        Yii::setAlias('@pathCover', realpath(dirname(__FILE__).'/../images'));

        $model = new Games();

        if ($model->load(Yii::$app->request->post()) ) {
            $file = UploadedFile::getInstance($model, 'gameCover');
            $model->createDate = date("Y-m-d H:i:s");
            $file->saveAs(Yii::getAlias('@pathCover').'/'. $file->name);
            
            $model->gameCover = $file->name;
            $model->creatorId=$session['idUser'];
            $model->save();
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            $allGenre = ArrayHelper::map(GameGenre::find()->all(), 'id','name');
            return $this->render('create', [
                'model' => $model,
                'genreList' => $allGenre,
            ]);
        }
    }
    // Html::activeDropDownList($model, 's_id',ArrayHelper::map(Standard::find()->all(), 's_id', 'name'))
    /**
     * Updates an existing Games model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $allGenre = ArrayHelper::map(GameGenre::find()->all(), 'id','name');
        $model = $this->findModel($id);
        $pic_name = $model->gameCover;
        Yii::setAlias('@pathCover', realpath(dirname(__FILE__).'/../images'));
        if ($model->load(Yii::$app->request->post())) 
        {
            $file = UploadedFile::getInstance($model, 'gameCover');
            if(is_object($file))
            {
                $file->saveAs(Yii::getAlias('@pathCover').'/'. $file->name);    
                $model->gameCover = $file->name;
            }else{
                $model->gameCover = $pic_name;
            }
            
            $model->save(false);
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            
            return $this->render('update', [
                'model' => $model,
                'genreList' => $allGenre,
            ]);
        }
    }

    /**
     * Deletes an existing Games model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    function actionSearch()
    {
        $model = $this->getAllData();
        // $model = new Games();
        $query = new Query;
        if(isset($_POST))
        {
            $model = Games::find()->where(['like', 'game.name', $_POST['games']['name'], ])->all();
        }
        return $this->render('searchList', [
                'model' => $model,
            ]);
    }

    /**
     * Finds the Games model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Games the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Games::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    
}
