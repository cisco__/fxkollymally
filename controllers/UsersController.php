<?php
namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\Users;
use app\models\GameComments;
use app\models\Games;
use yii\web\UploadedFile;
use kartik\social\Disqus;

class UsersController extends \yii\web\Controller
{
    public function get3data()
    {
        return Games::find()->orderBy(['id'=>SORT_DESC,])->limit(3)->all();
    }


    public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

    /*
    public function actionIndex()
    {
        return $this->render('index');
    }
    */

    public function actionLogin()
    {
        $session = Yii::$app->session;
        $session->open();
        if(!isset($session['idUser']) || empty($session['idUser']))
        {
        	$model = new Users;
        	if(isset($_POST['Users']))
    		{
    			$request = Yii::$app->request;
    			$data = Users::find()->where(['email' => $_POST['Users']['email'],'password' => $_POST['Users']['password']])->one();
    			if(isset($data))
    			{
    				$session = Yii::$app->session;			
    				$session->set('idUser',$data->id);
    				$session->set('nameUser',$data->name);	
    		        Yii::$app->response->redirect(array('/'));  		
    			}else{
    				Yii::$app->response->redirect(array('/'));
    			}	
    		}
            return $this->render('login', ['model' => $model,'auth' => ['class' => 'yii\authclient\AuthAction','successCallback' => [$this,'successCallback'],], ]);
        }elseif (isset($session['idUser'])) 
        {
            Yii::$app->response->redirect(array('/'));
        }
    }

    public function actionLogout()
    {
    	$session = Yii::$app->session;
    	// close a session
		$session->remove('idUser');
		$session->remove('nameUser');
		$session->destroy();
		$session->close();

    	Yii::$app->response->redirect(array('login'));
    }

    public function actionGetfromajax()
    {
    	$session = Yii::$app->session;
    	$request = Yii::$app->request;
    	$comments = $request->post('comments'); 
    	$gameID = $request->post('gameID'); 
        $model= new GameComments;
        $model->gameID=$gameID;
        $model->comments=$comments;
        $model->userID=$session['idUser'];
        $model->createDate=date('Y-m-d:H:i:s');
        $model->save(false);
        echo json_encode("OK");
    }

    public function actionCreate()
    {
        $model = new Users();
        Yii::setAlias('@pathCover', realpath(dirname(__FILE__).'/../images'));
        if ($model->load(Yii::$app->request->post()) ) 
        {
            $file = UploadedFile::getInstance($model, 'photo');
            // var_dump(Yii::getAlias('@pathCover').'/');
            $file->saveAs(Yii::getAlias('@pathCover').'/'. $file->name);
            $model->photo = $file->name;
            $model->save();
            return $this->redirect('login');
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

}

