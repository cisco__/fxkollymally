<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Games */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="block">
        <div class="block-bot">
          <div class="head">
            <div class="head-cnt"> 
              <h3><?= Html::encode($this->title) ?></h3>
              <div class="cl">&nbsp;</div>
            </div>
          </div>
          <div class="col-articles articles">
          
              <div class="block-bot">
                <div class="block-cnt">


    <?php $form = ActiveForm::begin(['options' => ['class' => 'cesco_form','enctype' => 'multipart/form-data']]) ?>
                    <div class="cl">&nbsp;</div>
                    <div class="fieldplace">
    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
                    </div>
                   
    <?php //$form->field($model, 'createDate')->textInput() ?>
<div class="fieldplace">
<?php if(isset($model->gameCover))
    {
      echo "<img src='http://".$_SERVER['SERVER_NAME']."/images/".$model->gameCover."' width=150 height=150 id='gambar' />";
    } ?>
    <?= $form->field($model, 'gameCover')->fileInput(['maxlength' => true]) ?>
    
</div>
<div class="fieldplace">
    <?= $form->field($model, 'descr')->textarea(['rows' => 6]) ?>
</div>
<div class="fieldplace">
    <?= $form->field($model, 'developer')->textInput(['maxlength' => true]) ?>
</div>
<div class="fieldplace">
    <?= $form->field($model, 'publisher')->textInput(['maxlength' => true]) ?>
</div>
<div class="fieldplace">
    <?= $form->field($model, 'youtubeLink')->textInput(['maxlength' => true]) ?>
</div>
<div class="fieldplace">
    <?php // $form->field($model, 'genreID')->textInput() ?>
<div class="form-group field-games-genreID required">
<label class="control-label" for="games-genreID">Game's Genre</label>
<div class="help-block"></div>
</div>    

<?php $dtgenreID = $model->genreID; ?>
    <select name="Games[genreID]">
              <option>--Please Select--</option>
         <?php  foreach ($genreList as $key => $value) {
                if($dtgenreID == $key)
                {
                  echo '<option selected value="'.$key.'">'.$value.'</option>  ';
                }else{
            ?>  
                <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
        <?php } } ?>
    </select>
</div>
<br /><br />
    <div class="fieldplace">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'submit btn-success' : 'submit btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
                </div>
              </div>   
          </div>
        </div>
      </div>


