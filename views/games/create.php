<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Games */

$this->title = 'Create Games';
$this->params['breadcrumbs'][] = ['label' => 'Games', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="games-create">
    <?= $this->render('_form', [
        'model' => $model,
        'genreList' => $genreList,
    ]) ?>

</div>
