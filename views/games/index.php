<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\gamesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Games';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="games-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    

    <?php /* GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            // 'id',
            'name',
            // 'createDate',
            // 'creatorId',
            'gameCover'=>array('format' => 'html','value' => function($data) 
                { 
                    return Html::a(Html::img('http://localhost'.Yii::$app->homeUrl.'images/'.$data['gameCover'],['width'=>'50']), ['games/view', 'id' => $data['id']], ['class' => 'profile-link']) ;
                }),
            // 'descr:ntext',
            // 'developer',
            // 'publisher',
            // 'genreID',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); */ ?>

</div>




<?php



/* @var $this yii\web\View */
/* @var $model app\models\Users */
/* @var $form yii\widgets\ActiveForm */

// $this->title = 'Login';
// $this->params['breadcrumbs'][] = $this->title;

?>

 
<div class="col-articles articles">
<div class="cl">&nbsp;</div>
<?php /* GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            // 'id',
            'name',
            // 'createDate',
            // 'creatorId',
            'gameCover'=>array('format' => 'html','value' => function($data) 
                { 
                    return Html::a(Html::img('http://localhost'.Yii::$app->homeUrl.'images/'.$data['gameCover'],['width'=>'50']), ['games/view', 'id' => $data['id']], ['class' => 'profile-link']) ;
                }),
            // 'descr:ntext',
            // 'developer',
            // 'publisher',
            // 'genreID',

            ['class' => 'yii\grid\ActionColumn'],
        ],
]); */ ?>
      
<?php
$x=1;
    foreach ($model as $key => $value) 
    { ?>
        <div class="article">
            <div class="image"> <a href="<?php echo Yii::$app->request->baseUrl.'games/'.$value->id; ?>"><img src="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/images/'.$value->gameCover; ?>" alt=""></a> </div>
            <h4><a href="<?php echo Yii::$app->request->baseUrl.'games/'.$value->id; ?>"><?php echo $value->name; ?></a></h4>
            <p class="console"><strong><?php echo $value->genre->name; ?></strong></p>
        </div>
<?php
    $x++;
    if($x > 3){$x=1;echo'<div class="cl">&nbsp;</div>';}
    }
?>
            <div class="cl">&nbsp;</div>
</div>