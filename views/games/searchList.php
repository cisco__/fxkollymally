<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\gamesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Games';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="games-index">

    <h1><?= Html::encode($this->title) ?></h1>
</div>

<div class="col-articles articles">
<div class="cl">&nbsp;</div>
      
<?php
$x=1;
    foreach ($model as $key => $value) 
    { ?>
        <div class="article">
            <div class="image"> <a href="<?php echo Yii::$app->request->baseUrl.'games/'.$value->id; ?>"><img src="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/images/'.$value->gameCover; ?>" alt=""></a> </div>
            <h4><a href="<?php echo Yii::$app->request->baseUrl.'games/'.$value->id; ?>"><?php echo $value->name; ?></a></h4>
            <p class="console"><strong><?php echo $value->genre->name; ?></strong></p>
        </div>
<?php
    $x++;
    if($x > 3){$x=1;echo'<div class="cl">&nbsp;</div>';}
    }
?>
            <div class="cl">&nbsp;</div>
</div>