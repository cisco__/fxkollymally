<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\gamesSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="games-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

     <?= // $form->field($model, 'id') ?>

    <?= $form->field($model, 'name') ?>

    <?= //$form->field($model, 'createDate') ?>

    <?= //$form->field($model, 'creatorId') ?>

    <?= $form->field($model, 'gameCover') ?>

    <?php // echo $form->field($model, 'descr') ?>

    <?php // echo $form->field($model, 'developer') ?>

    <?php // echo $form->field($model, 'publisher') ?>

    <?php // echo $form->field($model, 'genreID') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
