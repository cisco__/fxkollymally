<?php
$session = Yii::$app->session;          
                
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Games */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Games', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="col-articles articles">
<div class="cl">&nbsp;</div>

<?php if($model->creatorId == $session['idUser'])            
{ 
    echo Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn submit btn-primary']); 
}
?>
    <div class="article">
        <div class="image">
            <img src='<?php echo 'http://'.$_SERVER['SERVER_NAME']."/images/".$model->gameCover; ?>' alt="" />
        </div>
    </div>
    <div class="article">
        <label>Game's Name :</label><p style="color:green;"><?php echo $model->name; ?></p>
    </div>
    <br /><br /><br /><br />
    <div class="article">
        <label>Game's Description :</label><p style="color:green;"><?php echo $model->descr; ?></p>
    </div>
    <br /><br /><br /><br />
    <div class="article">
        <label>Game's Developer :</label><p style="color:green;"><?php echo $model->developer; ?></p>
    </div>
    <br /><br /><br /><br />
    <div class="article">
        <label>Game's Publisher :</label><p style="color:green;"><?php echo $model->publisher; ?></p>
    </div>
    <br /><br /><br /><br />
    <div class="videoWrapper">
    <!-- Copy & Pasted from YouTube -->
        <?php echo $model->youtubeLink; ?>
    </div>
    
    <br /><br /><br /><br />
    
  
<?php 
if($session['idUser'])
{
?>   
<textarea id='Isicomment' rows='4' cols='60' ></textarea>
<br /><br />
<button id='comment'>Comment Now</button>    
<br /><br />
<?php } ?>
<br /><br /><br /><br /><br /><br />
<h1>Comments</h1>
<br /><br />
<div>
<?php 
foreach ($comments as $key) {
?>
 <div class="tsc_clean_comment">
    <div class="comment_box fr">
      <p class="comment_paragraph" contenteditable="true"><?php echo $key->comments; ?></p>
    </div>
    <div class="avatar_box"> 
        <img src='<?php echo "http://".$_SERVER['SERVER_NAME']."/images/".$key->user->photo; ?>' height="50" width="50" alt="Avatar" class="avatar" />
        <p class="username"><?php echo $key->user->name; ?></p>
    </div>
    <div class="tsc_clear"></div>
</div>

<?php    
}
?>
</div>
<div class="cl">&nbsp;</div>
</div>
<script type="text/javascript">
$(function() {
    var gameID = '<?php echo $model->id; ?>';
    var userID = '<?php echo Yii::$app->session['idUser']; ?>';
    var url = '<?php echo "http://".$_SERVER["SERVER_NAME"]."/games"; ?>';
    var comment = "";
    $("#comment").click( function()
    {
        
        comment = $("#Isicomment").val();
        var formData = {comments:comment,game:gameID,user:userID,_csrf : '<?=Yii::$app->request->getCsrfToken()?>'};
        $.ajax({
                    url : url,
                    type: "POST",
                    data : formData,
                    success: function(data, textStatus, jqXHR)
                    {
                        location.reload(true);
                    },
                    error: function (jqXHR, textStatus, errorThrown)
                    {
                 
                    }
                });
    });
});
</script>