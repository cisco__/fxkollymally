<?php

    use yii\helpers\Html;
    use yii\bootstrap\Nav;
    use yii\bootstrap\NavBar;
    use yii\widgets\Breadcrumbs;
    use app\assets\AppAsset;
    use app\controllers\GamesController;


    AppAsset::register($this);
    // $session = new Session;
    $session = Yii::$app->session;
    $session->open();
?>
<?php $this->beginPage(); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>GamePortal</title>
<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href= "<?php echo Yii::$app->request->baseUrl; ?>/css/style.css" type="text/css" media="all" />
<!--[if IE 6]><link rel="stylesheet" href="assets/css/ie6-style.css" type="text/css" media="all" /><![endif]-->

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script src="<?php echo Yii::$app->request->baseUrl; ?>/js/tweecool.js"></script>
</head>
<body>

<!-- Page -->
<div id="page" class="shell">
  <!-- Header -->
  <div id="header">

    <div class="cl">&nbsp;</div>
    <!-- Logo -->
    <div id="logo">
      <h1><a href="<?php echo '/'; ?>">fundnel<span>game portal</span></a></h1>
      <p class="description">Fundnel Testing App</p>
    </div>
    <!-- / Logo -->
    <!-- Main Navigation -->
    <div id="main-nav">

    </div>
    <!-- / Main Navigation -->
    <div class="cl">&nbsp;</div>
    <!-- Sort Navigation -->
    <div id="sort-nav">
      <div class="bg-right">
        <div class="bg-left">
          <div class="cl">&nbsp;</div>
          <ul>
            <li class="first active first-active"><a href="/">Home</a><span class="sep">&nbsp;</span></li>
            <!-- <li><a href='<?php //echo "http://".$_SERVER['SERVER_NAME']; ?>/games'>Games</a><span class="sep">&nbsp;</span></li> -->
            <?php
              if(isset($session['nameUser']) || $session['nameUser']!= '')
              {
            ?>
                <li><a href="<?php echo "http://".$_SERVER['SERVER_NAME']; ?>/createGames">Create New Game Portal</a><span class="sep">&nbsp;</span></li>
                <li><a href="<?php echo "http://".$_SERVER['SERVER_NAME']; ?>/logout">Logout(<?php echo $session['nameUser']; ?>)</a><span class="sep">&nbsp;</span></li>
               <?php }else{ ?>
                <li><a href="<?php echo "http://".$_SERVER['SERVER_NAME']; ?>/login">Login</a><span class="sep">&nbsp;</span></li>
              <?php } ?>  
          </ul>
          <div class="cl">&nbsp;</div>
        </div>
      </div>
    </div>
    <!-- / Sort Navigation -->
  </div>
  <!-- / Header -->
  <!-- Main -->
    <div id="main">
    <div id="main-bot">
      <div class="cl">&nbsp;</div>
    <div id="content">
      
      <?= $content ?>
     
    </div>
    <!-- / Content -->
    <!--  -->
    <div id="sidebar">
      <!-- Search -->
      <div id="search" class="block">
        <div class="block-bot">
          <div class="block-cnt">
            <form action="<?php echo 'http://'.$_SERVER['SERVER_NAME']; ?>/games/search" method="post">
              <div class="cl">&nbsp;</div>
              <div class="fieldplace">
                <input type="text" class="field" value="Search" name="games[name]" title="Search" />
              </div>
              <input type="hidden" name="_csrf" value="<?=Yii::$app->request->getCsrfToken()?>" />
              <input type="submit" class="button" value="GO" />
              <div class="cl">&nbsp;</div>
            </form>
          </div>
        </div>
      </div>
      <!-- / Search -->
      
      <div class="block">
        <div class="block-bot">
          <div class="head">
            <div class="head-cnt">
              <h3>3 Latest Games</h3>
            </div>
          </div>
          <div class="image-articles articles">
            <div class="cl">&nbsp;</div>
            <?php $data = $this->context->get3data(); foreach ($data as $key => $value) { ?>
              <div class="article">
                <div class="cl">&nbsp;</div>
                <div class="image"> <a href="<?php echo Yii::$app->request->baseUrl.'games/'.$value->id; ?>"><img src="<?php echo 'http://'.$_SERVER['SERVER_NAME']."/images/".$value->gameCover; ?>" alt="" /></a> </div>
                <div class="cnt">
                  <h4><a href="<?php echo Yii::$app->request->baseUrl.'games/'.$value->id; ?>"><?php echo $value->name; ?></a></h4>
                  <p><?php echo $value->descr; ?></p>
                </div>
                <div class="cl">&nbsp;</div>
              </div>    
            <?php } ?>
            <div class="cl">&nbsp;</div>
            <!-- <a href="#" class="view-all">view all</a> -->
            <div class="cl">&nbsp;</div>
          </div>
        </div>
      </div>
      

      <div class="block">
        <div class="block-bot">
          <div class="head">
            <div class="head-cnt">
              <h3>Our Tweets</h3>
            </div>
          </div>
          <div class="image-articles articles">
            <div class="cl">&nbsp;</div>
              <div class="twitter-widget">
              <div id="tweecool"></div>            
              </div>
            <div class="cl">&nbsp;</div>
            <!-- <a href="#" class="view-all">view all</a> -->
            <div class="cl">&nbsp;</div>
          </div>
        </div>
      </div>
     
    </div>
    <!-- / Sidebar -->





    <div class="cl">&nbsp;</div>
    <!-- Footer -->
    <div id="footer">
      <div class="navs">
        <div class="navs-bot">
          
          <div class="cl">&nbsp;</div>
        </div>
      </div>
      <p class="copy">&copy; fundneltest.com</p>
    </div>
    <!-- / Footer -->
  </div>
</div>
<!-- / Main -->
</div>
<!-- / Page -->
</body>
<script type="text/javascript">
    $(function()
    {
        $('#tweecool').tweecool({
             username : 'gameloft', 
             limit : 6 
            });

        $('#signupTombol').on('click', function(event)
        {
              {
                window.open("createUser","_self");
              }
        });
  })
</script>
</html>