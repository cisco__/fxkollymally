<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>
 
      <div class="block">
        <div class="block-bot">
          <div class="head">
            <div class="head-cnt"> 
              <h3><?= Html::encode($this->title) ?></h3>
              <div class="cl">&nbsp;</div>
            </div>
          </div>
          <div class="col-articles articles">
          
              <div class="block-bot">
                <div class="block-cnt">
                <p>Please fill out the following fields to login:</p>
                  <?php $form = ActiveForm::begin([
        'id' => 'cesco-form',
        'options' => ['class' => 'cesco-form'],
        'fieldConfig' => [
            'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
            'labelOptions' => ['class' => 'col-lg-1 control-label'],
        ],
    ]); ?>
                    <div class="cl">&nbsp;</div>
                    <div class="fieldplace">
                      <?= $form->field($model, 'email') ?>
                    </div>

                    <div class="fieldplace">
                      <?= $form->field($model, 'password')->passwordInput() ?>
                    </div>
                    <br /><br />

                    <?= Html::submitButton('Login', ['class' => 'submit', 'name' => 'login-button']) ?>
                <?= Html::Button('SignUp', ['class' => 'submit', 'name' => 'signup-button','id' => 'signupTombol']) ?>
      
                    <div class="cl">&nbsp;</div>
                  <?php ActiveForm::end(); ?>                
                </div>
              </div>
          
            
        

        

        

    
          </div>
        </div>
      </div>

