
<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Users */
/* @var $form yii\widgets\ActiveForm */

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>
 
      <div class="block">
        <div class="block-bot">
          <div class="head">
            <div class="head-cnt"> 
              <h3><?= Html::encode($this->title) ?></h3>
              <div class="cl">&nbsp;</div>
            </div>
          </div>
          <div class="col-articles articles">
              <div class="block-bot">
                <div class="block-cnt">
                <p>Please fill out the following fields to login:</p>
                
                  <?php $form = ActiveForm::begin([
        'id' => 'signup-form',
        'options' => ['class' => 'cesco_form','enctype' => 'multipart/form-data'],
        'fieldConfig' => [
            'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
            'labelOptions' => ['class' => 'col-lg-1 control-label'],
        ],
    ]); ?>
                    <div class="cl">&nbsp;</div>
                    <div class="fieldplace">
                      <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
                    </div>

                    <div class="fieldplace">
                      <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="fieldplace">
                      <?= $form->field($model, 'password')->passwordInput(['maxlength' => true]) ?>
                    </div>
                    <div class="fieldplace">
                      <?= $form->field($model, 'photo')->fileInput(['maxlength' => true]) ?>
                    </div>
                    <br /><br />

<?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'submit btn-success' : 'btn btn-primary']) ?>                    <div class="cl">&nbsp;</div>
                  <?php ActiveForm::end(); ?>                
                </div>
              </div>   
          </div>
        </div>
      </div>

