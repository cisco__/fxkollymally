-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 03, 2016 at 09:37 PM
-- Server version: 5.5.44-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `gamesdb`
--

-- --------------------------------------------------------

--
-- Table structure for table `game`
--

CREATE TABLE IF NOT EXISTS `game` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `createDate` datetime NOT NULL,
  `creatorId` int(11) NOT NULL COMMENT 'This related with users',
  `gameCover` varchar(100) NOT NULL,
  `descr` text NOT NULL,
  `developer` varchar(50) NOT NULL,
  `publisher` varchar(50) NOT NULL,
  `genreID` int(11) NOT NULL,
  `youtubeLink` text,
  PRIMARY KEY (`id`),
  KEY `creatorId` (`creatorId`),
  KEY `genreID` (`genreID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=30 ;

--
-- Dumping data for table `game`
--

INSERT INTO `game` (`id`, `name`, `createDate`, `creatorId`, `gameCover`, `descr`, `developer`, `publisher`, `genreID`, `youtubeLink`) VALUES
(4, 'Time Crises 4', '2015-12-23 00:00:00', 1, 'time.jpg', 'Time Crisis 4 adalah nama permainan video rail shooter yang dikembangkan oleh Nex Entertainment dan dipublikasikan oleh Namco.', 'Nex', 'Namco', 1, '<iframe width="560" height="315" src="https://www.youtube.com/embed/bo-j2eyzfe4" frameborder="0" allowfullscreen></iframe>'),
(5, 'Street Fighter', '2015-12-25 11:36:32', 1, 'street.jpg', 'The coin-operated arcade game version was released in Japan on July 18, 2008, with North American arcades importing the machines by August.[12] The console versions for the PlayStation 3 and Xbox 360', 'Capcom', 'CapCom', 3, '<iframe width="560" height="315" src="https://www.youtube.com/embed/qeTO8LlVNsc" frameborder="0" allowfullscreen></iframe>'),
(6, 'Mortal Kombat', '2015-12-25 12:02:14', 1, 'Mortal_Kombat.png', 'Mortal Kombat X[b] is a fighting video game developed by NetherRealm Studios and published by Warner Bros. Interactive Entertainment. It is the tenth main installment in the Mortal Kombat video game series and was released on April 14, 2015 for Microsoft Windows, PlayStation 4, and Xbox One. NetherRealm studio''s mobile team developed a version for iOS and Android devices. Versions for the PlayStation 3 and Xbox 360 were originally planned for release, but both were later cancelled. Mortal Kombat X was officially announced in June 2014.', 'NetherRealm Studios', 'Warner Bros', 3, '<iframe width="560" height="315" src="https://www.youtube.com/embed/VlSR1FHt9NA" frameborder="0" allowfullscreen></iframe>'),
(27, 'GTA Five', '2016-01-01 14:38:30', 1, 'gta.jpg', 'Grand Theft Auto V adalah game aksi dan petualangan yang dikembangkan oleh Rockstar North dan di terbitkan oleh Rockstar Games. Game ini terbit pada tanggal 17 september 2013 untuk console Playstation 3 dan Xbox 360.', 'Rockstar Games', 'Rockstar Games', 1, '<iframe width="560" height="315" src="https://www.youtube.com/embed/_UB5hpk0iqg" frameborder="0" allowfullscreen></iframe>'),
(28, 'Counter Strike', '2016-01-01 14:44:43', 1, 'cs.jpg', 'Counter-Strike adalah permainan video tembak-menembak orang-pertama yang merupakan modifikasi dari permainan video Half-Life oleh Minh "Gooseman" Le dan Jess "Cliffe" Cliffe. ', 'Valve Corp', 'Sierra ', 1, '<iframe width="420" height="315" src="https://www.youtube.com/embed/xgQ4eRu02MI" frameborder="0" allowfullscreen></iframe>'),
(29, 'Mario Bros 2', '2016-01-01 14:55:03', 1, 'Mario-Bros.jpg', 'Permainan arkade yang dibuat dan diterbitkan oleh Nintendo pada tahun 1983. Permainan ini dibuat oleh Shigeru Miyamoto, pembuat seri permainan Mario.', 'Nintendo', 'Nintendo', 3, '<iframe width="560" height="315" src="https://www.youtube.com/embed/3XrnN4m_fgw" frameborder="0" allowfullscreen></iframe>');

-- --------------------------------------------------------

--
-- Table structure for table `gameComments`
--

CREATE TABLE IF NOT EXISTS `gameComments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gameID` int(11) NOT NULL,
  `comments` text NOT NULL,
  `userID` int(11) NOT NULL,
  `createDate` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `gameID` (`gameID`,`userID`),
  KEY `userID` (`userID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `gameComments`
--

INSERT INTO `gameComments` (`id`, `gameID`, `comments`, `userID`, `createDate`) VALUES
(3, 4, 'ksksk sksksks', 1, '0000-00-00 00:00:00'),
(4, 5, 'hollywood', 1, '2015-12-24 21:51:31'),
(5, 5, 'hollywood 2', 1, '2015-12-24 21:57:09'),
(9, 4, 'testing comment', 1, '2016-01-01 15:37:08'),
(10, 4, 'testing comment', 1, '2016-01-01 15:38:42'),
(11, 4, 'Comments = Lorem Ipsum adalah contoh teks atau dummy dalam industri percetakan dan penataan huruf atau typesetting. Lorem Ipsum telah menjadi standar contoh teks sejak tahun 1500an, saat seorang tukang cetak yang tidak dikenal mengambil sebuah kumpulan teks dan mengacaknya untuk menjadi sebuah buku contoh huruf. Ia tidak hanya bertahan selama 5 abad, tapi juga telah beralih ke penataan huruf elektronik, tanpa ada perubahan apapun. Ia mulai dipopulerkan pada tahun 1960 dengan diluncurkannya lembaran-lembaran Letraset yang menggunakan kalimat-kalimat dari Lorem Ipsum, dan seiring munculnya perangkat lunak Desktop Publishing seperti Aldus PageMaker juga memiliki versi Lorem Ipsum.', 6, '2016-01-02 09:21:45');

-- --------------------------------------------------------

--
-- Table structure for table `gameGenre`
--

CREATE TABLE IF NOT EXISTS `gameGenre` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `gameGenre`
--

INSERT INTO `gameGenre` (`id`, `name`) VALUES
(1, 'Shooter'),
(2, 'Racing'),
(3, 'Adventure');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `email` varchar(30) NOT NULL,
  `password` varchar(200) NOT NULL,
  `photo` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `photo`) VALUES
(1, 'Francesco Xavier Kolly Mally', 'francescokm@gmail.com', 'secretpassword', 'pasfoto.jpg'),
(6, 'Ivana Dione', 'ivanadione@gmail.com', 'secret', 'kuda.jpg');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `game`
--
ALTER TABLE `game`
  ADD CONSTRAINT `game_ibfk_1` FOREIGN KEY (`creatorId`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `game_ibfk_2` FOREIGN KEY (`genreID`) REFERENCES `gameGenre` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `gameComments`
--
ALTER TABLE `gameComments`
  ADD CONSTRAINT `gameComments_ibfk_1` FOREIGN KEY (`gameID`) REFERENCES `game` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `gameComments_ibfk_2` FOREIGN KEY (`userID`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
